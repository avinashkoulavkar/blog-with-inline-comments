json.data do 
	json.id @article.id
	json.title @article.title

	json.paragraphs @article.paragraphs do |paragraph|
		json.id paragraph.id
		json.text paragraph.text

		json.comments paragraph.comments do |comment|
			json.id comment.id
			json.comment comment.comment
		end
	end
end