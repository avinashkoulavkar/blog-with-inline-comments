json.articles @articles.each do |article|
	json.id article.id
	json.paragraph article.paragraphs.first.text
end
