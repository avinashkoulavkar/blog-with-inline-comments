class CommentsController < ApplicationController

	def show
		#In case there are more than 5 comments and user want to load other comments
		respond_to do |format|
			@comments = Comment.where(paragraph_id: params[:paragraph_id])
		end
	end

	def create
		comment = Comment.create(comment_params)
		render json: {id: comment.id}
	end

	private

	def comment_params
		params.permit(:comment, :paragraph_id)
	end
end
