class ArticleController < ApplicationController
	def index
		#Return Article title, and first paragraph 
		params[:page] ||= 1
		params[:per_page] ||= 5
		@articles = Article.order(created_at: :desc).paginate(:page => params[:page], :per_page => params[:per_page])

	end

	def new
		#New Article view page
		
	end

	def create
		#Create new article with artciel params
		article = Article.create!(title: article_params[:title])
		params[:paragraph].each do |paragraph| 
			Paragraph.create(text: paragraph, article_id: article.id)
		end
		render json: {id: article.id}
	end

	def show
		@article = Article.find params[:id]	
		render json: {error: "not_found", error_description: "No record found with given id"}, status: :not_found if @article.blank?
	end

	private

	def article_params
		params.permit(:title, :paragraph)
	end
end
