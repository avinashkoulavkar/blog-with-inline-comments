// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require turbolinks
//= require_tree .

//= require jquery
//= require bootstrap-sprockets

function processArticle() {
	$('form').submit(false);
	var str = document.getElementById("articleBody").value;
  if(str == "") {
    alert("At least one paragraph must be there!");
    return false;
  }
	var n = str.split("\n\n");
	var paragraph = [];
	for(var x in n) {
		paragraph[x] = n[x];
	}
	var title = document.getElementById("articleTitle").value;

	$.ajax({  
        type: "POST",  
        url: "/article",  
        data: {'title': title, paragraph: paragraph},  
        success: function(dataString) {  
            alert("added successfully!");
            document.getElementById("articleBody").value = "";
            document.getElementById("articleTitle").value = "";
        },
       error: function(jqXHR, textStatus, errorThrown) {
       		alert("Error while processing article")
       }
    });  
}