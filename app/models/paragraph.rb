# == Schema Information
#
# Table name: paragraphs
#
#  id             :integer          not null, primary key
#  text           :text
#  article_id     :integer
#  foreign_key_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Paragraph < ApplicationRecord
	belongs_to :article
	has_many :comments
end
