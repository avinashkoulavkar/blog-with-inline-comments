# == Schema Information
#
# Table name: comments
#
#  id           :integer          not null, primary key
#  comment      :string
#  paragraph_id :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Comment < ApplicationRecord
	belongs_to :paragraph
end
